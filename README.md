# screentogif

Grava sua tela e converte para o formato de imagem animada (GIF) usando ffmpeg.

# Exemplo de uso:
	$ ./screentogif.sh [-s | --slop]
	$ Pressione CTRL+C para parar a gravação
	$ Converter a partir de (em segundos) : 1
	$ Quadro por segundo ( de preferência < 24 ) : 10
	$ Largura (pixels - altura será definida automaticamente) : 800

	-s | --slop
		Parâmetro opcional. Permite selecionar uma área específica da
		tela a ser gravada. Esse recurso depende da ferramenta slop.
		Para instalar o slop: # apt install slop
